# TheLibraryProject #

A quick implementaion of a Library system. Completed for Citi App-dev training. 

### What is this repository for? ###

* This Library implementation allows users to create differnt media items and track their status in the Library system. Items can be checked-in and checked-out, and new items can be added to the Library at anytime. Currently only 4 media types are supported. 
* 0.0.001

### How do I get set up? ###

* Take a look at Library.java!

### Contribution guidelines ###

* Please don't contribute to this. 

### Who do I talk to? ###

* (c) psc 2020