package libraryproject;

// abstract class, each object must be a specific media type 
public abstract class LibraryObject {

    // True if the object is checked out of the library, false otherwise
    public boolean checkedOut = false;

    public String title;
    public String author;
    public int publishDate;


    // all created LibraryObjects share the idGenerator to ensure no duplicate IDs
    public static int idGenerator = 0;

    // each object has its own ID for use in the Library class
    public int id = 0;

    // deafault constructor
    public LibraryObject(String title, int publishDate, String author) {
        this.title = title;
        this.publishDate = publishDate;
        this.author = author;
        
        // increase the static generator at each newly created LibraryObject
        idGenerator++;
        this.id = idGenerator;
    }

    // creates a string representation
    public String toString() {
        return "Library Object: " + title + ". " + "Created by " + author + ". " + "Published in "
        + publishDate;
    }

    public static void main(String[] args) {
        LibraryObject testObject = new Book("spam", 5, "eggs");
        System.out.println(testObject.checkedOut);
        System.out.println(testObject.checkedOut);

    }
}
