package libraryproject;


// a book LibraryObject
public class Book extends LibraryObject {
    
    public String type = "Book";

    // default contructor to populate attributes for easy testing
    public Book() {
        super("spam", 1998, "eggs");
    }


    // String representation of the Book LibraryObject
    public String toString() {
        String output = "";
        output = output + "~~~~~~~~~~~~\n";
        output = output + "Media Type: " + this.type + "\n";
        output = output + "Title: " + this.title + "\n";
        output = output + "Author: " + this.author + "\n";
        output = output + "Published: " + this.publishDate + "\n"; 
        output = output + "ID: " + this.id + "\n";
        if (checkedOut) {
            output = output + "Currently Checkedout \n";
        } else {
            output = output + "Currently Available \n";
        }

        return output;
    }

    // second constructor for more precise data entry
    public Book(String name, int publishDate, String author) {
        super(name, publishDate, author);
    }
}