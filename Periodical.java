package libraryproject;

public class Periodical extends LibraryObject {
    
    public String type = "Periodical";

    public Periodical() {
        super("popular science", 2018, "eggs");
    }



    public String toString() {
        String output = "";
        output = output + "~~~~~~~~~~~~\n";
        output = output + "Media Type: " + this.type + "\n";
        output = output + "Title: " + this.title + "\n";
        output = output + "Editor: " + this.author + "\n";
        output = output + "Published: " + this.publishDate + "\n"; 
        output = output + "ID: " + this.id + "\n";

        return output;
    }


    public Periodical(String name, int publishDate, String author) {
        super(name, publishDate, author);
    }
}