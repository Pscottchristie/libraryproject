package libraryproject;

// a CD LibraryObject
public class CD extends LibraryObject {
    
    public String type = "CD";

    // default constructor
    public CD() {
        super("the long road", 2006, "nickelback");
    }


    // gives a string representation on the CD
    public String toString() {
        String output = "";
        output = output + "~~~~~~~~~~~~\n";
        output = output + "Media Type: " + this.type + "\n";
        output = output + "Title: " + this.title + "\n";
        output = output + "Artist: " + this.author + "\n";
        output = output + "Released: " + this.publishDate + "\n"; 
        output = output + "ID: " + this.id + "\n";
        if (checkedOut) {
            output = output + "Currently Checkedout \n";
        } else {
            output = output + "Currently Available \n";
        }

        return output;
    }

    // second constructor
    public CD(String name, int publishDate, String author) {
        super(name, publishDate, author);
    }
}