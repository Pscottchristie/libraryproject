package libraryproject;

public class Library {

    // main array to store library contents
    // this implementation could be greatly improved by using a dictionary hee for efficient lookups and deletions
    LibraryObject[] libContents;

    // a simple counter to keep track of the total number of items in the library
    public int totalItems = 0;

    // a constructor that creates a library of the given size
    public Library(int size) {
        this.libContents = new LibraryObject[size];
    }


    // a toString method to print the library in a readable form
    // possible extensions to this method include filters based on media type
    public String toString() {
        String message = "\nThis Library contains " + totalItems + " item(s): \n";
        String output = "";
        for (int i = 0; i < this.libContents.length; i++) {
            if (this.libContents[i] == null) {
                continue;
            }
            else {
                output = output + this.libContents[i].toString();
                output = output + "\n";
            }
        }
        return message + output;
    }


    // a method to add a LibraryObject to the library
    public void addItem(LibraryObject newObject) {
        for (int i = 0; i < libContents.length; i++) {
            if (libContents[i] == null) {
                libContents[i] = newObject;
                break;
            } 
        }
        totalItems++;
    }


    // a method to remove a LibraryObject from the library
    public void removeItem(int idTarget) {
        LibraryObject[] proxyArray = new LibraryObject[libContents.length - 1];
        for (int i = 0, k = 0; i < libContents.length; i++) {
            if (libContents[i] == null) {
                proxyArray[k++] = libContents[i];
            }
            else {
                if (libContents[i].id == idTarget) {
                    continue;
                }
                proxyArray[k++] = libContents[i];
            }
            libContents = proxyArray;
        }   
        totalItems--;
    }


    // a method to checkout a LibraryObject e.g. change its attribute to checked-out
    public boolean checkout(int idTarget) {
        for (int i = 0; i < libContents.length; i++) {
            if (libContents[i] == null) {
                continue;
            }
            else {
                if (libContents[i].id == idTarget && libContents[i].checkedOut == false) {
                    libContents[i].checkedOut = true;
                    return true;
                }   
            }
        }
        System.out.println("\nID not found in Library or the item is already checked-out. \n");
        return false;   
    }


    // a method to checkin a LibraryObject e.g. change its attribute to checked-in
    public boolean checkin(int idTarget) {
        for (int i = 0; i < libContents.length; i++) {
            if (libContents[i] == null) {
                continue;
            }
            else {
                if (libContents[i].id == idTarget && libContents[i].checkedOut == true) {
                    libContents[i].checkedOut = false;
                    return true;
                }   
            }
        }
        System.out.println("\nID not found in Library or the item is already checked-in. \n");
        return false;   
    }

    
    public static void main(String[] args) {
        Library l = new Library(5);
        Book b = new Book();
        DVD d = new DVD();
        CD c = new CD();
        Periodical p = new Periodical();

        l.addItem(d);
        l.addItem(b);
        l.addItem(c);
        l.addItem(p);
        l.addItem(b);

        System.out.println(l);



    
    }

}