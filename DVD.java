package libraryproject;

public class DVD extends LibraryObject {
    
    public String type = "DVD";

    public DVD() {
        super("harry potter", 2008, "m night shamamamalan");
    }



    public String toString() {
        String output = "";
        output = output + "~~~~~~~~~~~~\n";
        output = output + "Media Type: " + this.type + "\n";
        output = output + "Title: " + this.title + "\n";
        output = output + "Director: " + this.author + "\n";
        output = output + "Released: " + this.publishDate + "\n"; 
        output = output + "ID: " + this.id + "\n";
        if (checkedOut) {
            output = output + "Currently Checkedout \n";
        } else {
            output = output + "Currently Available \n";
        }

        return output;
    }


    public DVD(String name, int publishDate, String author) {
        super(name, publishDate, author);
    }
}